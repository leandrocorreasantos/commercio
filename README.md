# flaskshop

Um sistema de loja virtual que utiliza Flask + Bootstrap + AngularJs

## Instruções aos desenvolvedores

- Após clonar o repositório, faça checkout no ramo develop
```bash
git checkout develop
```

- Crie um ambiente virtual com o virtualenv
```bash
python3 -m venv venv
```

- Carregue o ambiente virtual
```bash
source venv/bin/activate
```

- Carregue as dependências necessárias para o desenvolvimento 
```bash
pip install -r requirements-local.txt
```

- Crie um ramo para a alteração que deseja fazer
```bash
git checkout -b new_feature
```

- Antes de enviar o código para o repositório, baixe as últimas alterações do ramo develop e mescle com as alterações feitas

```bash
git checkout develop
git pull
git checkout new_feature
git merge develop
```

- Envie o ramo com as alterações para o repositório
```bash
git push origin new_feature
```

- No painel do gitlab, abra um Merge Request de suas alterações para o ramo develop




